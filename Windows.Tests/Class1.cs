﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Windows.Tests
{
    public class Class1
    {
        public Class1()
        {

        }

        [Fact]
        public void Test()
        {
            int expected = 1;
            int actual = 1;
            Assert.Equal(expected, actual);
        }
    }
}
