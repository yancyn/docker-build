# Build with Docker
Learn to build .NET project with docker using gitlab ci.

## References
- https://github.com/alexellis/guidgenerator-aspnet
- https://gitlab.com/gitlab-org/gitlab-ci-yml/
- https://dotnetthoughts.net/building-dotnet-with-gitlab-ci/
- https://github.com/forrestab/dotnet-gitlab-ci
